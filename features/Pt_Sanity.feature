@sanity
Feature: Weloveshopping Sanity Check

Scenario: Validate Search by keyword when data found
    Given User has [PtHomepage] open
    When User re-enters 'mac book' to [txt-search]
    And User clicks the [btn-search]
    Then The [search_result] is visible

Scenario: Validate Search by keyword when data NOT found
    Given User has [PtHomepage] open
    When User re-enters 'Atiikleiglldoogp;' to [txt-search]
    And User clicks the [btn-search]
    Then The [number_not_found] is visible
    And The [number_not_found] shows '0'

Scenario: Validate Register page: Open create new account page.
    Given User has [PtHomepage] open
    When User clicks the [create_account]
    Then The browser shows [PtSignupUser] page

@facebook
Scenario: Open facebook welove fanpage
      Given User has [PtHomepage] open
      When User clicks the [facebook-button]
      Then The page url is 'https://www.facebook.com/weloveshoppingbuzz'

Scenario: Validate product price : Exclusive Wetrust Shop: [Stock > 0, Price = SHOW, price > 0] The result should show [ADD-TO-CART] button, NOT show [DISCOUNT-PRICE].
    Given User has [PtProductW26536053] open
    Then The [product_price] contains '500'
    And The [box_quantity] is visible
    And The [quantity] shows '1'
    And The [btn_add_cart] is visible

Scenario: Validate product price : Platinum Wetrust Shop: [Stock > 0, Price = SHOW, price > 0, DISCOUNT = YES] The result should show [DISCOUNT-PRICE] & [ADD-TO-CART] BUTTON.
    Given User has [PtProductW26224326] open
    Then The [product_price] contains '600'
    And The [initial_price] contains '1,200'
    And The [box_quantity] is visible
    And The [quantity] shows '1'
    And The [btn_add_cart] is visible


Scenario: Validate Box Items List
    Given User has [PtCategory] open
    Then The [box-items-list] exists
    And The page has '50' items of [amount-item-list]

Scenario: Verify Highlight Box: Display items.
    Given User has [PtHomepage] open
    Then The [banner-hot-campaign] is visible
    And The page has '6' items of [count-item-highlight]

Scenario: Verify Shop Recommend: Display items.
    Given User has [PtHomepage] open
    Then The [box-shop-recommend] is visible
    And The page has '4' items of [shop-item]
    And The [shop-item] is visible
    And The [shop-info] is visible

Scenario: Verify Hit Trend Box: Display items.
    Given User has [PtHomepage] open
    Then The page has '4' items of [tab-content-hit-product]
    And The page has '3' items of [pic-item-hit-product]

Scenario: Verify Footer Banner: Display items.
    Given User has [PtHomepage] open
    Then The [footer-banner-1] exists
    Then The [footer-banner-2] exists
    Then The [footer-banner-3] exists

Scenario Outline: Verify Category Menu Left-side and total number: Display item.
    Given User has [PtHomepage] open
    Then The [<menu_category>] is visible
    And The [<number_total_product>] is visible

Examples: Verify Categoroy Left side Menu & Number Total product
| menu_category | number_total_product |
| menu_clothes | total_clothes |
| menu_accessory | total_accessory |
| menu_cosmetics | total_cosmetics |
| menu_momandkid | total_momandkid |
| menu_toy | total_toy |
| menu_song | total_song |
| menu_art | total_art |
| menu_book | total_book |
| menu_camera | total_camera |
| menu_electric | total_electric |
| menu_mobile | total_mobile |
| menu_furniture | total_furniture |

Scenario: Validate information required on register account page: User does NOT enter email.
    Given User has [PtSignupUser] open
    When User clears value on the [email]
    And User enters 'Invalid password' to [password]
    And User enters 'Invalid password' to [re_password]
    And User enters '0891234567' to [phone]
    And User clicks the [accept_condition]
    And User clicks the [btn_register]
    Then The [error_email] shows 'กรุณากรอกอีเมล'

Scenario: Validate information required on register account page: User does NOT enter password.
    Given User has [PtSignupUser] open
    When User enters 'usertest.111@gmail.com' to [email]
    And User clears value on the [password]
    And User enters 'Invalid_password' to [re_password]
    And User enters '0891234567' to [phone]
    And User clicks the [accept_condition]
    And User clicks the [btn_register]
    Then The [error_password] shows 'กรุณากรอกรหัสผ่าน'
    And The [error_re_password] shows 'กรุณากรอกให้ตรงกับรหัสผ่าน'

Scenario: Validate information required on register account page: User does NOT enter re-password.
    Given User has [PtSignupUser] open
    When User enters 'usertest.111@gmail.com' to [email]
    And User enters 'Invalid_password' to [password]
    And User clears value on the [re_password]
    And User enters '0891234567' to [phone]
    And User clicks the [accept_condition]
    And User clicks the [btn_register]
    Then The [error_re_password] shows 'กรุณากรอกรหัสผ่าน'

Scenario: Validate information required on register account page: User does NOT enter mobile.
    Given User has [PtSignupUser] open
    When User enters 'usertest.111@gmail.com' to [email]
    And User enters 'Invalid_password' to [password]
    And User enters 'Invalid_password' to [re_password]
    And User clears value on the [phone]
    And User clicks the [accept_condition]
    And User clicks the [btn_register]
    Then The [error_phone] shows 'กรุณากรอกเบอร์โทรศัพท์มือถือ'

Scenario: Validate information required on register account page: User does NOT checked [agreed-terms-and-Condition].
    Given User has [PtSignupUser] open
    When User enters 'usertest.111@gmail.com' to [email]
    And User enters 'Invalid_password' to [password]
    And User enters 'Invalid_password' to [re_password]
    And User enters '0891234567' to [phone]
    And User clicks the [btn_register]
    Then The [msg_accept_condition] contains 'กรุณายอมรับนโยบายและเงื่อนไขการให้บริการ'
