# -*- coding: utf-8 -*-

##
## Implement additional steps here.
##

from behave import *
from project_cylon.World import *
from project_cylon.Logger import *
import time

##
## Given step definitions
##


@step ("User delete all items in cart")
def step_impl(context):
    page = World.find_page('PtCart').go()
    elements = World.find_element('delete_pid').get_items()

    for element in elements:
        element.click()
        World.get_alert().accept()

## 1. Logout
## 2. Login
## 3. Verify Current login whether it's show username.
@step ("User login with '{Username}' and '{Password}'")
def step_impl(context, Username, Password):
  World.find_page('PtLogin').go()
  World.find_page('PtLogin').wait_for_loading()
  World.find_element('username').send_keys(Username)
  World.find_element('password').send_keys(Password)
  World.find_element('btn_login').click()
  World.find_page('PtHomepage').wait_for_loading()

@step ("The current login shows '{Username}'")
def step_impl(context, Username):
  World.find_page('PtHomepage').go()
  World.find_page('PtHomepage').wait_for_loading()
#  time.sleep(4)
  element = World.find_element('current_login')
  if Username in element.value:
    return True
  else:
    Logger.failed("Verify username.", element.value, Username)


@step ("User re-enters '{Value}' to [{ElementName}]")
def step_impl(context, Value, ElementName):
    Element = World.find_element(ElementName)
    Element.send_keys_by_script("")
    Element.send_keys(Value)

@step ("User logout")
def step_impl(context):
  World.find_page('PtLogout').go()
  World.find_page('PtLogin').wait_for_loading()

@step ("User logout then open login page")
def step_impl(context):
  World.find_page('PtLogout').go()
  World.find_page('PtLogin').wait_for_loading()
  World.find_page('PtLogin').go()

@step ("User waits for '{Time}' seconds")
def step_impl(context,Time):
	time.sleep(float(Time))

@step ("User currently not logged in")
def step_impl(context):
  World.find_page('PtHomepage').go()
  World.find_page('PtHomepage').wait_for_loading()
  #World.find_element('menu-')
#  time.sleep(5)
  if World.find_element('menu-my-account').exists:
    #World.find_element('menu-my-account').MouseOver()
    World.find_element('menu-logout').click()
