
from project_cylon.Logger import *
from project_cylon.PageFactory import *
from project_cylon.CommonSteps import *

from project_cylon.World import World as world

def before_all(context):
    Logger.tracebacklimit(0)

    browser = "firefox"
    pageobject_files = "./pageobjects/*.yaml"

    site = "default"

    if hasattr(context.config, 'site') and context.config.site is not None:
        site = context.config.site

    domain = PageFactory.get_domain("./config.yaml", site)

    if PageFactory.check_yaml_syntax(pageobject_files) == True:
        world.pages = PageFactory.create_pages(pageobject_files, domain)
        world.open_browser(browser)
    else:
        Logger.failed("Stop running.")

def after_all(context):
    world.close_browser()
