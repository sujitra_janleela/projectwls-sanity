#!/bin/bash

# Purpose: Generate Object and run Automated test
# Author: Lek
# Created Date: 2013-10-09
# Updated Date: 2013-10-25: Refactor, add option 3,4, help
#               2013-10-28: Add ClearPY() function


# Print out Menu Function
Menu(){
echo "----------------[`basename ${0}`]-------------------"
echo " USAGE $./Run.sh <option>          "


echo ""
echo " h:: SHOW MENU"
echo "-------------------------------------------"
}


# Run behave Function
Run(){
    #echo "ENTER RUN FUNCTION"
    behave --logging-level INFO --color --no-source --no-skipped    
}
Runtag(){
    #echo "ENTER RUN FUNCTION"
    behave --logging-level INFO --color --no-source --no-skipped --tags "$Input2"    
}

RunDry(){
    behave --logging-level INFO --color --no-source --no-skipped -d --tags "$Input2"
}

Kill_Firefox(){
    ps -ef | grep "firefox" | grep "foreground" | awk '{print $2}' | xargs kill
}

# Main Command
Menu
OPTION="$1"
Input2="$2"
echo "TAG: $TAG"

Kill_Firefox
Run






